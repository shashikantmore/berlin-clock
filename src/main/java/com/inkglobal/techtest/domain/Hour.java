/**
 * 
 */
package com.inkglobal.techtest.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * @author shash
 * 
 */
public class Hour implements Serializable {

	private static final Logger log = Logger.getLogger(Hour.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 4027653356852945072L;

	private List<ColorCode> firstRowHours = new ArrayList<ColorCode>(4);
	private List<ColorCode> secondRowHours = new ArrayList<ColorCode>(4);

	/**
	 * Constructor to initialize hours board
	 * 
	 * @param hour
	 */
	public Hour(int hour) {
		log.info("Setting up hours: " + hour);
		while (hour > 4) {
			if ((hour / 5) > 0) {
				firstRowHours.add(ColorCode.R);
				log.debug("Turning " + firstRowHours.size()
						+ " light on, in first row");
				hour -= 5;
			}
		}
		for (int index = firstRowHours.size(); index < 4; index++) {
			firstRowHours.add(ColorCode.O);
			log.debug("Turning " + index + " light off, in first row");
		}
		for (int index2 = 0; index2 < 4; index2++) {
			if (index2 < hour) {
				secondRowHours.add(ColorCode.R);
				log.debug("Turning " + index2 + " light on, in second row");
			} else {
				secondRowHours.add(ColorCode.O);
				log.debug("Turning " + index2 + " light off, in first row");
			}
		}
	}

	@Override
	public String toString() {
		StringBuffer stringBuffer = new StringBuffer();
		for (int index = 0; index < firstRowHours.size(); index++) {
			stringBuffer.append(firstRowHours.get(index));
		}
		stringBuffer.append(" ");
		for (int index = 0; index < secondRowHours.size(); index++) {
			stringBuffer.append(secondRowHours.get(index));
		}
		return stringBuffer.toString();
	}

}
