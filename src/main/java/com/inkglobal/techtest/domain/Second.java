/**
 * 
 */
package com.inkglobal.techtest.domain;

import java.io.Serializable;

import org.apache.log4j.Logger;

/**
 * @author shash
 * 
 */
public class Second implements Serializable {

	private static final Logger log = Logger.getLogger(Second.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 803992291165962948L;

	private ColorCode code = ColorCode.O;

	/**
	 * @param i
	 */
	public Second(int sec) {
		log.info("Setting up seconds: " + sec);
		if ((sec % 2) == 0) {
			code = ColorCode.Y;
		} else {
			code = ColorCode.O;
		}
	}

	@Override
	public String toString() {
		return code.getValue() + "";
	}
}
