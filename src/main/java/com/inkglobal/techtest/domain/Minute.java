/**
 * 
 */
package com.inkglobal.techtest.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * @author shash
 * 
 */
public class Minute implements Serializable {

	private static final Logger log = Logger.getLogger(Minute.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 7723089348185291503L;

	private List<ColorCode> firstRowMinutes = new ArrayList<ColorCode>(11);
	private List<ColorCode> secondRowMinutes = new ArrayList<ColorCode>(4);

	/**
	 * Constructor to initialize minute board
	 * 
	 * @param i
	 */
	public Minute(int minute) {
		log.info("Setting up minutes: " + minute);
		while (minute > 5) {
			if ((minute / 5) > 0) {
				if (firstRowMinutes.size() == 2 || firstRowMinutes.size() == 5
						|| firstRowMinutes.size() == 8) {
					firstRowMinutes.add(ColorCode.R);
					log.debug("Turning " + firstRowMinutes.size()
							+ " light on, in first row");

				} else {
					firstRowMinutes.add(ColorCode.Y);
					log.debug("Turning " + firstRowMinutes.size()
							+ " light on, in first row");
				}
				minute -= 5;
			}
		}
		for (int index = firstRowMinutes.size(); index < 11; index++) {
			firstRowMinutes.add(ColorCode.O);
			log.debug("Turning " + index + " light off, in first row");
		}
		for (int index2 = 0; index2 < 4; index2++) {
			if (index2 < minute) {
				secondRowMinutes.add(ColorCode.Y);
				log.debug("Turning " + index2 + " light on, in first row");
			} else {
				secondRowMinutes.add(ColorCode.O);
				log.debug("Turning " + index2 + " light off, in first row");
			}
		}
	}

	@Override
	public String toString() {
		StringBuffer stringBuffer = new StringBuffer();
		for (int index = 0; index < firstRowMinutes.size(); index++) {
			stringBuffer.append(firstRowMinutes.get(index));
		}
		stringBuffer.append(" ");
		for (int index = 0; index < secondRowMinutes.size(); index++) {
			stringBuffer.append(secondRowMinutes.get(index));
		}
		return stringBuffer.toString();
	}
}
