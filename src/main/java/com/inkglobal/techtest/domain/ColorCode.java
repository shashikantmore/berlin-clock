/**
 * 
 */
package com.inkglobal.techtest.domain;

/**
 * Color code enum provides value for YELLO, RED and OFF
 * 
 * @author shass
 */
public enum ColorCode {

	Y('Y'), R('R'), O('O');

	private char value;

	private ColorCode(char value) {
		this.value = value;
	}

	public char getValue() {
		return value;
	}
}
