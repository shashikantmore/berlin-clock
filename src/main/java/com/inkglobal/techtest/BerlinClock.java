package com.inkglobal.techtest;

public interface BerlinClock {
	/**
	 * Get the time in Berlin clock format
	 * 
	 * @return
	 */
	String getTime();
}
