/**
 * 
 */
package com.inkglobal.techtest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import com.inkglobal.techtest.domain.Hour;
import com.inkglobal.techtest.domain.Minute;
import com.inkglobal.techtest.domain.Second;

/**
 * Berlin clock implementation, to get time in Berlin clock format.
 * 
 * @author shash
 * 
 */
public class BerlinClockImpl implements BerlinClock {

	private static final Logger log = Logger.getLogger(BerlinClockImpl.class);

	private Hour hour;

	private Minute minute;

	private Second second;

	private Date date;

	private DateFormat format = new SimpleDateFormat("HH:mm:ss");

	/**
	 * Default constructor to take time as current time.
	 */
	public BerlinClockImpl() {
		log.info("Default constructor");
		Calendar calendar = Calendar.getInstance();
		date = calendar.getTime();
		hour = new Hour(calendar.get(Calendar.HOUR_OF_DAY));
		minute = new Minute(calendar.get(Calendar.MINUTE));
		second = new Second(calendar.get(Calendar.SECOND));
	}

	public BerlinClockImpl(Date paramDate) {
		log.info("Date based constructor");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(paramDate);
		date = calendar.getTime();
		hour = new Hour(calendar.get(Calendar.HOUR_OF_DAY));
		minute = new Minute(calendar.get(Calendar.MINUTE));
		second = new Second(calendar.get(Calendar.SECOND));
	}

	/**
	 * Constructor to take time in HH:mm:ss format.
	 * 
	 * @param string
	 * @throws ParseException
	 *             when input string is not a valid time.
	 */
	public BerlinClockImpl(String string) {
		log.info("String based constructor");
		try {
			date = format.parse(string);
		} catch (ParseException e) {
			throw new IllegalArgumentException(e.getMessage());
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		hour = new Hour(calendar.get(Calendar.HOUR_OF_DAY));
		minute = new Minute(calendar.get(Calendar.MINUTE));
		second = new Second(calendar.get(Calendar.SECOND));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.assignment.BerlinClock#getTime()
	 */
	public String getTime() {
		return format.format(date) + " " + second.toString() + " "
				+ hour.toString() + " " + minute.toString();
	}

}
