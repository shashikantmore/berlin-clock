package com.inkglobal.techtest;

import java.util.Calendar;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

public class BerlinClockTest {

	private BerlinClock berlinClock;

	/**
	 * Method to test default constructor based Berlin clock.
	 */
	@Test
	public void test() {
		berlinClock = new BerlinClockImpl();
		berlinClock.getTime();
	}

	@Test
	public void testExplicitTime() {
		berlinClock = new BerlinClockImpl("13:17:01");
		Assert.assertEquals("13:17:01 O RROO RRRO YYROOOOOOOO YYOO",
				berlinClock.getTime());
	}

	@Test
	public void testExplicitBoundaryTime() {
		berlinClock = new BerlinClockImpl("23:59:59");
		Assert.assertEquals("23:59:59 O RRRR RRRO YYRYYRYYRYY YYYY",
				berlinClock.getTime());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testException() {
		berlinClock = new BerlinClockImpl("23:59:BB");
		berlinClock.getTime();
	}

	@Test
	public void testCurrentTime() {
		berlinClock = new BerlinClockImpl(Calendar.getInstance().getTime());
		berlinClock.getTime();
	}

	/**
	 * This looks invalid test because in the clock we never get time 24:00:00,
	 * instead its equivalent to 00:00:00
	 */
	@Test
	public void testExplicitBoundaryTime2() {
		berlinClock = new BerlinClockImpl("24:00:00");
		String result = berlinClock.getTime();
		Assert.assertNotEquals("24:00:00 Y RRRR RRRR OOOOOOOOOOO OOOO", result);
	}

	@Test
	public void testExplicitBoundaryTime3() {
		berlinClock = new BerlinClockImpl("00:00:00");
		Assert.assertEquals("00:00:00 Y OOOO OOOO OOOOOOOOOOO OOOO",
				berlinClock.getTime());
	}

	@After
	public void tearDown() {
		berlinClock = null;
	}
}
