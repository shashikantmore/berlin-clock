Documentation:

Design: The problem statement includes the representation of current HH:MM:SS to convert to Berlin Clock display. The objects for hours, minutes and second remain same in this representation. The translation is been done at the creation itself as representation of object won’t exists without newer values.

Implementation: Berlin clock has very distinct way of showing time and it seriously need separate calculations. The implementation done using several classes as Hour, Minute and Second which get initialised as a part of BerlinClock.
Hour: It has two rows where first row represent each lamp as 5 hours and second row as one hour.
Minute: It has two rows where first row represent each lamp as 5 minutes and second row as one minute.
Second: It glows on every even number of second.
